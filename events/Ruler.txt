namespace = anb_ruler

# Any culture that lives longer 120 years max should be given this flag
country_event = {
	id = anb_ruler.0
	title = anb_ruler.0.t
	desc = anb_ruler.0.d
	picture = DIPLOMACY_eventPicture
	
    trigger = {
        has_dlc = "Rights of Man"
        NOT = { ruler_has_personality = immortal_personality } 
        NOT = { has_ruler_flag = set_immortality }
        OR = {
            culture_group = elven
            culture_group = dwarven
            culture_group = gnomish
			#or lich
        }
    }
   
	option = {
		name = anb_ruler.0.a

		clear_scripted_personalities = yes
        add_ruler_personality = immortal_personality
        set_ruler_flag = set_immortality
	}
}

# Death event
# When a ruler has an Immortal flag, and is of a culture with a long, but not immortal lifespan, this event will strip them of the flag up when they reach the "Old" age for their culture
country_event = {
	id = anb_ruler.1
	title = anb_ruler.1.t
	desc = anb_ruler.1.d
	picture = DIPLOMACY_eventPicture
	
    trigger = {
        has_dlc = "Rights of Man"
        ruler_has_personality = immortal_personality 
        OR = {
            # Dwarfs
            AND = {
                OR = {
                    culture_group = dwarven
                }
                ruler_age = 200
            }
            # Gnomes
            AND = {
                culture_group = gnomish
                ruler_age = 250
            }
            # Elves
            AND = {
                culture_group = elven
                ruler_age = 400
            }
        }
		#Future Additions: Liches
    }
   
	option = {
		name = anb_ruler.1.a
		
        remove_ruler_personality = immortal_personality
	}
}

# Age Decadence
country_event = {
	id = anb_ruler.2
	title = anb_ruler.2.t
	desc = anb_ruler.2.d
	picture = DIPLOMACY_eventPicture
	
    trigger = {
        has_dlc = "Rights of Man"
        ruler_has_personality = immortal_personality
		ruler_age = 100
    }
	
	#is_triggered_only = yes
	
	mean_time_to_happen = {
		months = 240
		modifier = {
			factor = 0.9
			ruler_age = 200
		}
		modifier = {
			factor = 0.8
			ruler_age = 250
		}
		modifier = {
			factor = 0.7
			ruler_age = 300
		}
		modifier = {
			factor = 0.5
			ruler_age = 350
		}
		
		modifier = {
			factor = 0.9
			ruler_has_personality = careful_personality 
		}
		modifier = {
			factor = 0.9
			ruler_has_personality = zealot_personality 
		}
		modifier = {
			factor = 1.5
			ruler_has_personality = free_thinker_personality 
		}
		modifier = {
			factor = 1.2
			ruler_has_personality = scholar_personality 
		}
	}
   
	option = {	#Am I out of touch? No, it's the younger races who are wrong.
		name = anb_ruler.2.a
		if = {
			limit = {
				OR = {
					NOT = { adm = 0}
					NOT = { dip = 0}
					NOT = { mil = 0}
				}
			}
		}
		
		random_list = {
			30 = {
				decrease_ruler_adm_effect = yes
			}
			30 = {
				decrease_ruler_dip_effect = yes
			}
			30 = {
				decrease_ruler_mil_effect = yes
			}
			10 = {
				#Nothing make a tooltip
			}
		}
	}
    
    option = {	#Reminisce about the glory days
		name = anb_ruler.2.b
		if = {
			limit = {
				NOT = { prestige = -80}
			}
		}
		
        add_prestige = -45	#may be a bit too high
	}
    
    option = {	#The world moves too fast... I must contemplate on this.
		name = anb_ruler.2.c
		if = {
			limit = {
				NOT = { stability = -3}
			}
		}
		
        add_stability = -3
	}
}

# Age Revival
country_event = {
	id = anb_ruler.3
	title = anb_ruler.3.t
	desc = anb_ruler.3.d
	picture = DIPLOMACY_eventPicture
	
    trigger = {
        has_dlc = "Rights of Man"
        ruler_has_personality = immortal_personality
		#culture_group = elven
		ruler_age = 100
    }
	
	#is_triggered_only = yes
	
	mean_time_to_happen = {
		months = 140
		modifier = {
			factor = 0.8
			ruler_has_personality = free_thinker_personality 
		}
		modifier = {
			factor = 0.9
			ruler_has_personality = scholar_personality 
		}
		# modifier = {
			# factor = 1.5
			# ruler_age = 250
		# }
		# modifier = {
			# factor = 2
			# ruler_age = 300
		# }
		# modifier = {
			# factor = 3
			# ruler_age = 350
		# }
	}
   
	option = {	#We must be open to new change and ideas.
		name = anb_ruler.3.a
		
		increase_ruler_adm_effect = yes
	}
    
    option = {	#There's a whole new world out there, as an elder race we must lead the way!
		name = anb_ruler.3.b
		
		increase_ruler_dip_effect = yes
	}
    
    option = {	#I've seen enough war to know where we're headed.
		name = anb_ruler.3.c
		
        increase_ruler_mil_effect = yes
	}
}

