###########################################################
# Events for the Mages Estate
#
# written by Tus3
###########################################################

namespace = mages_estate_events

#Secret event to allow hiring war wizards with the trait!
country_event = {
	id = mages_estate_events.0
	title = mages_estate_events.0.t
	desc = mages_estate_events.0.d
	picture = DHIMMI_ESTATE_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_estate = estate_mages
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	immediate = {
		hidden_effect = {
			set_saved_name = {
				key = root_name_male
				type = advisor
				scope = ROOT
			}
			set_saved_name = {
				key = root_name_female
				type = advisor
				female = yes
				scope = ROOT
			}
		}
	}
	

	option = {
		name = mages_estate_events.0.a #Ok
		if = {
			limit = {
				army_tradition = 0.00
				NOT = { army_tradition = 20 }
			}
			random_list = {
				50 = {
					define_general = {
						shock = 6
						fire = 1
						manuever = 0
						siege = 3
						trait = war_wizard_personality
						name = root_name_male
					}
				}
				50 = {
					define_general = {
						shock = 6
						fire = 1
						manuever = 0
						siege = 3
						trait = war_wizard_personality
						female = yes
						name = root_name_female
					}
				}
			}
		}
		
		if = {
			limit = {
				army_tradition = 20
				NOT = { army_tradition = 40 }
			}
			random_list = {
				50 = {
					define_general = {
						shock = 7
						fire = 2
						manuever = 0
						siege = 4
						trait = war_wizard_personality
						name = root_name_male
					}
				}
				50 = {
					define_general = {
						shock = 7
						fire = 2
						manuever = 0
						siege = 4
						trait = war_wizard_personality
						female = yes
						name = root_name_female
					}
				}
			}
		}
		
		if = {
			limit = {
				army_tradition = 40
				NOT = { army_tradition = 60 }
			}
			random_list = {
				50 = {
					define_general = {
						shock = 8
						fire = 3
						manuever = 0
						siege = 5
						trait = war_wizard_personality
						name = root_name_male
					}
				}
				50 = {
					define_general = {
						shock = 8
						fire = 3
						manuever = 0
						siege = 5
						trait = war_wizard_personality
						female = yes
						name = root_name_female
					}
				}
			}
		}
		
		if = {
			limit = {
				army_tradition = 60
				NOT = { army_tradition = 80 }
			}
			random_list = {
				50 = {
					define_general = {
						shock = 9
						fire = 4
						manuever = 0
						siege = 5
						trait = war_wizard_personality
						name = root_name_male
					}
				}
				50 = {
					define_general = {
						shock = 9
						fire = 4
						manuever = 0
						siege = 5
						trait = war_wizard_personality
						female = yes
						name = root_name_female
					}
				}
			}
		}
		
		if = {
			limit = {
				army_tradition = 80
				NOT = { army_tradition = 100 }
			}
			random_list = {
				50 = {
					define_general = {
						shock = 9
						fire = 4
						manuever = 0
						siege = 6
						trait = war_wizard_personality
						name = root_name_male
					}
				}
				50 = {
					define_general = {
						shock = 9
						fire = 4
						manuever = 0
						siege = 6
						trait = war_wizard_personality
						female = yes
						name = root_name_female
					}
				}
			}
		}
		
		clear_saved_name = root_name_male
		clear_saved_name = root_name_female
	}
}

#Advancement of the Mages
country_event = {
	id = mages_estate_events.1
	title = mages_estate_events.1.t
	desc = mages_estate_events.1.d
	picture = DHIMMI_ESTATE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {

		has_estate = estate_mages
		any_owned_province = {
			has_estate = estate_mages
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_ADVANCES
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_ADVANCES_20
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_DECLINES
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	

	option = {
		name = mages_estate_events.1.a #Ok
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGES_ADVANCE
			influence = 10
			duration = 5475
		}
	}
}

#Decline of Mages Power
country_event = {
	id = mages_estate_events.2
	title = mages_estate_events.2.t
	desc = mages_estate_events.2.d
	picture = DHIMMI_ESTATE_eventPicture
	
	trigger = {

		has_estate = estate_mages
		any_owned_province = {
			has_estate = estate_mages
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_ADVANCES
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_ADVANCES_20
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_DECLINES
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.2.a #Ok
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGES_DECLINE
			influence = -10
			duration = 5475
		}
	}
}

#Mages accusing Artificers of breaking tradition and attempting to prison magic
#Mages vs Artificers
country_event = {
	id = mages_estate_events.3
	title = mages_estate_events.3.t
	desc = mages_estate_events.3.d
	picture = DEBATE_REPUBLICAN_eventPicture
	
	trigger = {
		has_estate = estate_mages
		has_estate = estate_artificers
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGIC_DEBATE_MAGES
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGIC_DEBATE_ARTIFICERS
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	is_triggered_only = yes
	
	
	
	option = {
		name = mages_estate_events.3.a #Support Artificers
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_mages
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_artificers
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -15
		}
		add_estate_loyalty = {
			estate = estate_artificers
			loyalty = 15
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGIC_DEBATE_ARTIFICERS
			influence = -5
			duration = 3650
		}
	}
	option = {
		name = mages_estate_events.3.b #Support Mages
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.2
				NOT = {
					estate_loyalty = {
						estate = estate_artificers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_artificers
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_artificers
			loyalty = -15
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 15
		}
		add_estate_influence_modifier = {
			estate = estate_artificers
			desc = EST_VAL_MAGIC_DEBATE_MAGES
			influence = -5
			duration = 3650
		}
	}
}

#Succession: Magical heir or non-magical one the guy petitions to make his heir his younger child thats magical
#Mages vs Nobles
country_event = {
	id = mages_estate_events.4
	title = mages_estate_events.4.t
	desc = mages_estate_events.4.d
	picture = NEW_HEIR_eventPicture
	
	trigger = {
		has_estate = estate_mages
		estate_influence = {
			estate = estate_nobles
			influence = 40
		}
		estate_influence = {
			estate = estate_mages
			influence = 25
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGICAL_NOBILITY_SUPPORTED
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_TRADITIONAL_NOBILITY_SUPPORTED
			}
		}
		OR = {
			any_owned_province = {
				has_estate = estate_nobles
				OR = {
					superregion = escann_superregion
					superregion = western_cannor_superregion
				}
			}
			has_reform = magical_elite_reform
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	is_triggered_only = yes
	
	option = {
		name = mages_estate_events.4.a #Succession is succession, no matter what.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_mages
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_nobles
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_TRADITIONAL_NOBILITY_SUPPORTED
			influence = -10
			duration = 3650
		}
		add_estate_influence_modifier = {
			estate = estate_nobles
			desc = EST_VAL_TRADITIONAL_NOBILITY_SUPPORTED
			influence = 10
			duration = 3650
		}
	}
	option = {
		name = mages_estate_events.4.b #Accept the petition
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				has_reform = magical_elite_reform
			}
			modifier = {
				factor = 0.2
				NOT = {
					estate_loyalty = {
						estate = estate_nobles
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_nobles
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGICAL_NOBILITY_SUPPORTED
			influence = 10
			duration = 3650
		}
		add_estate_influence_modifier = {
			estate = estate_nobles
			desc = EST_VAL_MAGICAL_NOBILITY_SUPPORTED
			influence = -10
			duration = 3650
		}
	}
}

#Magical experiment gone wrong
country_event = {
	id = mages_estate_events.5
	title = mages_estate_events.5.t
	desc = mages_estate_events.5.d
	picture = TEMPORAL_RIFT_eventPicture
	
	trigger = {
		has_estate = estate_mages
		OR = {
			any_owned_province = {
				development = 10
				is_capital = no
				is_overseas = no
				is_territory = no
			}
			any_owned_province = {
				is_capital = no
				is_overseas = no
				has_estate = estate_mages
				is_territory = no
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGICAL_SPELL_GONE_WRONG
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			has_country_modifier = mage_organization_decentralized
		}
	}
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					development = 10
					is_capital = no
					is_overseas = no
					is_territory = no
				}
				set_province_flag = magical_spell_gone_wrong
				save_event_target_as = magical_spell_gone_wrong_province
			}
			random_owned_province = {
				limit = {
					is_capital = no
					is_overseas = no
					has_estate = estate_mages
					is_territory = no
				}
				set_province_flag = magical_spell_gone_wrong
				save_event_target_as = magical_spell_gone_wrong_province
			}
		}
	}
	
	option = {
		name = mages_estate_events.5.a #Enforce stricter rules spellcasting
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGICAL_SPELL_GONE_WRONG
			influence = -20
			duration = 3650
		}
		random_owned_province = {
			limit = {
				has_province_flag = magical_spell_gone_wrong
			}
			add_province_modifier = { 
				name = magical_spell_gone_wrong
				duration = 1825
			}
		}
	}
	option = {
		name = mages_estate_events.5.b #Punish the wrongdoer
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGICAL_SPELL_GONE_WRONG
			influence = -20
			duration = 7300
		}
		random_owned_province = {
			limit = {
				has_province_flag = magical_spell_gone_wrong
			}
			add_province_modifier = { 
				name = magical_spell_gone_wrong
				duration = 3650
			}
		}
	}
}

#Conclave of Magi - this is a reactionary event if mages are getting too weak
country_event = {
	id = mages_estate_events.6
	title = mages_estate_events.6.t
	desc = mages_estate_events.6.d
	picture = DIET_eventPicture
	
	trigger = {

		has_estate = estate_mages
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGE_CONCLAVE_HELD
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 2
			NOT = {
				estate_influence = {
					estate = estate_mages
					influence = 30
				}
			}
		}
	}
	
	option = {
		name = mages_estate_events.6.a #Let them do their meeting
		ai_chance = {
			factor = 50
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGE_CONCLAVE_HELD
			influence = 20
			duration = 5475
		}
	}
	option = {
		name = mages_estate_events.6.b #Prevent the conclave at all costs!
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				estate_influence = {
					estate = estate_mages
					influence = 30
				}
			}
			modifier = {
				factor = 1.5
				estate_loyalty = {
					estate = estate_mages
					loyalty = 40
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -20
		}
	}
}

#Mage of Renown
country_event = {
	id = mages_estate_events.7
	title = mages_estate_events.7.t
	desc = mages_estate_events.7.d
	picture = ADVISOR_eventPicture
	
	trigger = {
		has_estate = estate_mages

		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_RENOWNED_MAGE_SUPPORTED
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.25
			has_country_modifier = mage_organization_decentralized
		}
	}
	
	option = {
		name = mages_estate_events.7.a #Yep, they're from here.
		ai_chance = {
			factor = 50
		}
		add_prestige = 10
	}
	option = {
		name = mages_estate_events.7.b #Reward and support them
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						estate = estate_mages
						loyalty = 60
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
		}
		add_years_of_income = -0.25
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 10
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_RENOWNED_MAGE_SUPPORTED
			influence = 5
			duration = 5475
		}
	}
}

#Mages financial trouble
country_event = {
	id = mages_estate_events.8
	title = mages_estate_events.8.t
	desc = mages_estate_events.8.d	#they want subsidizes to push research and shit
	picture = BANKRUPTCY_eventPicture
	
	trigger = {

		has_estate = estate_mages
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_SAVED_FROM_FINANCIAL_TROUBLE
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_FINANCIAL_TROUBLE
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			has_country_modifier = mage_organization_centralized
		}
	}
	
	option = {
		name = mages_estate_events.8.a #Give them some money
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_mages
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
		}
		add_years_of_income = -0.33
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 10
		}
	}
	option = {
		name = mages_estate_events.8.b #You're on your own!
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_FINANCIAL_TROUBLE
			influence = -15
			duration = 5475
		}
	}
}

#Complaining Magess
country_event = {
	id = mages_estate_events.9
	title = mages_estate_events.9.t
	desc = mages_estate_events.9.d
	picture = COSSACK_ESTATE_UPSET_eventPicture
	
	trigger = {

		has_estate = estate_mages
		NOT = {
			estate_loyalty = {
				estate = estate_mages
				loyalty = 45
			}
		}
		any_owned_province = {
			has_estate = estate_mages
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_INDEPENDENCE
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MagesS_LEAVING
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.9.a #
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_mages
						loyalty = 35
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_mages
					influence = 60
				}
			}
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGES_INDEPENDENCE
			influence = 10
			duration = 7300
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 10
		}
		every_owned_province = {
			limit = {
				has_estate = estate_mages
			}
			add_local_autonomy = 33
		}
	}
	option = {
		name = mages_estate_events.9.b #
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_mages
					influence = 70
				}
			}
			modifier = {
				factor = 0
				NOT = { manpower_percentage = 0.8 }
			}
		}
		add_yearly_manpower = -0.25
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MagesS_LEAVING
			influence = -10
			duration = 5475
		}
	}
}

#The People's Mage: Mage School founded (local community helps too)
country_event = {
	id = mages_estate_events.10
	title = mages_estate_events.10.t
	desc = mages_estate_events.10.d
	picture = GREAT_BUILDING_eventPicture
	
	trigger = {
		has_estate = estate_mages
		any_owned_province = {
			development = 15
			is_capital = no
			is_overseas = no
			NOT = { has_estate = yes }
			is_territory = no
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					development = 10
					is_capital = no
					is_overseas = no
					NOT = { has_estate = yes }
					has_seat_in_parliament = no
					is_territory = no
					is_state_core = ROOT		
				}
				set_province_flag = mage_school_built
				save_event_target_as = mage_school_built_province
			}
		}
	}
	
	option = {
		name = mages_estate_events.10.a #Allow it
		ai_chance = {
			factor = 50
		}
		random_owned_province = {
			limit = {
				has_province_flag = mage_school_built
			}
			set_estate = estate_mages
			add_base_tax = 1
			clr_province_flag = mage_school_built
		}
	}
	option = {
		name = mages_estate_events.10.b # Reverse this development
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				estate_influence = {
					estate = estate_mages
					influence = 70
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		random_owned_province = {
			limit = {
				has_province_flag = mage_school_built
			}
			add_province_modifier = { 
				name = peoples_mage_rejected
				duration = 3650
			}
		}
	}
}

#Demands at high influence
#Advisor accused of dark magic
country_event = {
	id = mages_estate_events.11
	title = mages_estate_events.11.t
	desc = mages_estate_events.11.d
	picture = ACCUSATION_eventPicture
	
	trigger = {

		has_estate = estate_mages
		estate_influence = {
			estate = estate_mages
			influence = 70
		}
		has_advisor = yes
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			has_country_modifier = mage_organization_centralized
		}
		modifier = {
			factor = 1.5
			has_country_modifier = mage_organization_magisterium
		}
	}
	
	option = {
		name = mages_estate_events.11.a #I think I know my advisors
		ai_chance = {
			factor = 50
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -20
		}
	}
	option = {
		name = mages_estate_events.11.b #Execute the advisor
		ai_chance = {
			factor = 50
		}
		kill_advisor = random
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 10
		}
	}
}

#Magess demand more autonomy
country_event = {
	id = mages_estate_events.12
	title = mages_estate_events.12.t
	desc = mages_estate_events.12.d
	picture = REFORM_eventPicture
	
	trigger = {

		has_estate = estate_mages
		estate_influence = {
			estate = estate_mages
			influence = 70
		}
		any_owned_province = {
			has_estate = estate_mages
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGES_INDEPENDENCE
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_Mages_DEMANDS_REBUFFED
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.12.a #
		ai_chance = {
			factor = 0
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGES_INDEPENDENCE
			influence = 10
			duration = 7300
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 10
		}
		every_owned_province = {
			limit = {
				has_estate = estate_mages
			}
			add_local_autonomy = 33
		}
	}
	option = {
		name = mages_estate_events.12.b #
		ai_chance = {
			factor = 1
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGES_DEMANDS_REBUFFED
			influence = -10
			duration = 5475
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -15
		}
	}
}

#Nice things can also come from strong and loyal Magess.
#Bonus for high influence + high loyalty

#High Loyalty - Sage Advice
country_event = {
	id = mages_estate_events.13
	title = mages_estate_events.13.t
	desc = mages_estate_events.13.d
	picture = CHURCH_ESTATE_1_eventPicture
	
	trigger = {

		has_estate = estate_mages
		estate_loyalty = {
			estate = estate_mages
			loyalty = 70
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.13.a #Excellent advice.
		random_list =  {
			25 = {
				add_adm_power = 100
			}
			25 = {
				add_dip_power = 100
			}
			25 = {
				add_mil_power = 100
			}
			25 = {
				add_adm_power = 100
				add_dip_power = 100
				add_mil_power = 100
			}
		}
	}
}


#Talented Battlemage Cohort
country_event = {
	id = mages_estate_events.14
	title = mages_estate_events.14.t
	desc = mages_estate_events.14.d			#as in a generation of battlemages loyal to the cause is enlisted
	picture = ELECTION_REPUBLICAN_eventPicture
	
	trigger = {
		has_estate = estate_mages
		estate_loyalty = {
			estate = estate_mages
			loyalty = 60
		}
		estate_influence = {
			estate = estate_mages
			influence = 60
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 0.75
			has_country_modifier = mage_organization_centralized
		}
		modifier = {
			factor = 0.75
			has_country_modifier = mage_organization_magisterium
		}
		modifier = {
			factor = 0.75
			is_at_war = yes
		}
	}
	
	option = {
		name = mages_estate_events.14.a #Magic Missile!
		add_country_modifier = {
			name = talented_battlemage_cohort
			duration = 7300
		}
	}
}


#Organization Specific

#Decentralized
#New Magical Guild Members
country_event = {
	id = mages_estate_events.15
	title = mages_estate_events.15.t
	desc = mages_estate_events.15.d		
	picture = STREET_SPEECH_eventPicture
	
	trigger = {

		has_estate = estate_mages
		has_country_modifier = mage_organization_decentralized
		
		#This is to make sure this only fires when they are weak
		estate_influence = {
			estate = estate_mages
			influence = 20
		}
		NOT = {
			estate_influence = {
				estate = estate_mages
				influence = 50
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_DECENTRALIZED_NEW_GUILD_MEMBERS
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.15.a #Good!
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_DECENTRALIZED_NEW_GUILD_MEMBERS
			influence = 10
			duration = 3650
		}
	}
	
	option = {
		name = mages_estate_events.15.b #Supress new recruitment
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				estate_loyalty = {
					estate = estate_mages
					loyalty = 50
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_DECENTRALIZED_SUPRESSING_NEW_GUILD_MEMBERS
			influence = -5
			duration = 3650
		}
	}
}

#Mages gone Wild (run amok)
country_event = {
	id = mages_estate_events.16
	title = mages_estate_events.16.t
	desc = mages_estate_events.16.d		
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {

		has_estate = estate_mages
		has_country_modifier = mage_organization_decentralized
		
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_DECENTRALIZED_MAGES_RUNNING_AMOK
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.16.a #Punish the Mage Guilds
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				estate_loyalty = {
					estate = estate_nobles
					loyalty = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_DECENTRALIZED_MAGES_RUNNING_AMOK
			influence = -15
			duration = 3650
		}
	}
	
	option = {
		name = mages_estate_events.16.b #Mages will be mages
		ai_chance = {
			factor = 50
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_DECENTRALIZED_MAGES_RUNNING_AMOK
			influence = -15
			duration = 7300
		}
	}
}

#Centralized
#Syllabus needs updating
country_event = {
	id = mages_estate_events.17
	title = mages_estate_events.17.t
	desc = mages_estate_events.17.d		
	picture = BIG_BOOK_eventPicture
	
	trigger = {

		has_estate = estate_mages
		has_country_modifier = mage_organization_centralized
		
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_CENTRALIZED_OUTDATED_SYLLABUS
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.17.a #Update the syllabus
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						estate = estate_nobles
						loyalty = 60
					}
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = 20
		}
		add_adm_power = -100
		add_years_of_income = -0.25
	}
	
	option = {
		name = mages_estate_events.17.b #It's fine as it is
		ai_chance = {
			factor = 50
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CENTRALIZED_OUTDATED_SYLLABUS
			influence = -5
			duration = 7300
		}
	}
}

#Magical Elite dislike subsidized students
country_event = {
	id = mages_estate_events.18
	title = mages_estate_events.18.t
	desc = mages_estate_events.18.d		
	picture = BURGHER_ESTATE_eventPicture
	
	trigger = {

		has_estate = estate_mages
		has_estate = estate_nobles
		OR = {
			has_country_modifier = mage_organization_centralized
			has_country_modifier = mage_organization_magisterium
		}
		
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_CENTRALIZED_SCHOLARSHIPS_FOR_THE_POOR
			}
		}			
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = mages_estate_events.18.a #Magic is about merit not birthright
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -10
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CENTRALIZED_SCHOLARSHIPS_FOR_THE_POOR
			influence = 10
			duration = 7300
		}
	}
	
	option = {
		name = mages_estate_events.18.b #It needs to stay with the elite
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						estate = estate_nobles
						loyalty = 60
					}
				}
			}
			modifier = {
				factor = 1.5
				has_reform = magical_elite_reform
			}
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 10
		}
	}
}


#Magisterium
#Magisterium disagrees with country's direction
country_event = {
	id = mages_estate_events.19
	title = mages_estate_events.19.t
	desc = mages_estate_events.19.d		
	picture = CHURCH_ESTATE_UPSET_eventPicture
	
	trigger = {

		has_estate = estate_mages
		has_country_modifier = mage_organization_magisterium
		
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 1.5
			A85 = {
				NOT = {
					has_opinion = {
						who = ROOT
						value = 100
					}
				}
			}
		}
	}
	
	option = {
		name = mages_estate_events.19.a #Assuage the Magisterium
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				prestige = 0
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -10
		}
		add_dip_power = -33
	}
	
	option = {
		name = mages_estate_events.19.b #Who cares?
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				estate_loyalty = {
					estate = estate_nobles
					loyalty = 40
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -20
		}
		add_prestige = -15
		if = {
			limit = {
				exists = A85
			}
			A85 = {
				add_opinion = {
					who = ROOT
					modifier = magisterium_disagrees_with_direction
					years = 20
				}
			}
		}
	}
}

#Magisterium sends support
country_event = {
	id = mages_estate_events.20
	title = mages_estate_events.20.t
	desc = mages_estate_events.20.d		
	picture = DHIMMI_ESTATE_eventPicture
	
	trigger = {

		has_estate = estate_mages
		has_country_modifier = mage_organization_magisterium
		
		#This is to make sure this only fires when they are weak
		NOT = {
			estate_influence = {
				estate = estate_mages
				influence = 30
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGISTERIUM_MAGISTERS_SENT_SUPPORT
			}
		}	
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_mages
				modifier = EST_VAL_MAGISTERIUM_REJECTED_MAGISTER_SUPPORT
			}
		}	
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
		modifier = {
			factor = 2
			A85 = {
				has_opinion = {
					who = ROOT
					value = 150
				}
			}
		}
	}
	
	option = {
		name = mages_estate_events.20.a #Appreciated
		add_stability = 1
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGISTERIUM_MAGISTERS_SENT_SUPPORT
			influence = 20
			duration = 3650
		}
	}
	
	option = {
		name = mages_estate_events.20.b #Reject the new Magisters
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_mages
					influence = 30
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_mages
			loyalty = -30
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_MAGISTERIUM_REJECTED_MAGISTER_SUPPORT
			influence = -10
			duration = 3650
		}
	}
}

#Local Revolt
province_event = {
	id = mages_estate_events.21
	title = mages_estate_events.21.t
	desc = mages_estate_events.21.d
	picture = CHURCH_ESTATE_UPSET_eventPicture
	
	is_triggered_only = yes

	option = {
		name = mages_estate_events.21.a #Ok.
		
	}
}

