namespace = flavor_smallcountry

#Small Country Revolt
country_event = {
	id = flavor_smallcountry.1
	title = "flavor_smallcountry.1.t"
	desc = "flavor_smallcountry.1.d"
	picture = ANGRY_MOB_eventPicture

	major = yes
	fire_only_once = yes
	
	#is_triggered_only = yes
	
	trigger = {
		is_year = 1550
		NOT = { is_year = 1650 }
		ai = yes
		was_player = no
		NOT = { exists = A97 }
		NOT = { tag = A97 }
		NOT = { tag = A12 }	#Beepeck cant revolt
		is_free_or_tributary_trigger = yes
		num_of_cities = 6
		any_owned_province = {
			region = small_country_region
			OR = {
				culture_group = halfling
				#has_province_modifier = halfling_minority_coexisting_large
				#has_province_modifier = halfling_minority_coexisting_small
				#has_province_modifier = halfling_minority_oppressed_large
				#has_province_modifier = halfling_minority_oppressed_small
				is_core = A97
			}
		}
		capital_scope = {
			NOT = { region = small_country_region }
		}
		NOT = { culture_group = halfling }
	}
	
	mean_time_to_happen = {
		months = 2000
		modifier = {
			factor = 0.75
			OR = {
				NOT = { accepted_culture = imperial_halfling }
				NOT = { accepted_culture = bluefoot_halfling }
				NOT = { accepted_culture = redfoot_halfling }
			}
		}
		modifier = {
			factor = 0.75
			num_of_owned_provinces_with = {
				value = 2
				region = small_country_region
				OR = {
					culture_group = halfling
				}
			}
		}
		modifier = {
			factor = 0.75
			num_of_owned_provinces_with = {
				value = 4
				region = small_country_region
				OR = {
					culture_group = halfling
				}
			}
		}
		modifier = {
			factor = 0.75
			num_of_owned_provinces_with = {
				value = 6
				region = small_country_region
				OR = {
					culture_group = halfling
				}
			}
		}
		modifier = {
			factor = 0.75
			num_of_owned_provinces_with = {
				value = 8
				region = small_country_region
				OR = {
					culture_group = halfling
				}
			}
		}
	}

	immediate = {
		hidden_effect = {
			every_owned_province = {
				limit = {
					region = small_country_region
					OR = {
						culture_group = halfling
						is_core = A97
					}
					is_capital = no
				}
				discover_country = A97
				add_core = A97
			}
			release = A97
			A97 = {
				change_government = republic
			}
			every_owned_province = {
				limit = {
					region = small_country_region
				}
				add_permanent_claim = A97
			}
		}
	}
	
	option = {
		name = "flavor_smallcountry.1.a"
		A97 = {
			declare_war_with_cb = {
				who = ROOT
				casus_belli = cb_independence_war
			}
		}
		hidden_effect = {
			A97 = {
				country_event = { id = flavor_smallcountry.4 days = 1 }
			}
			every_country = {
				limit = {
					NOT = { tag = ROOT }
					NOT = { tag = A97 }
					any_owned_province = {
						region = small_country_region
					}
				}
				country_event = { id = flavor_smallcountry.5 days = 10 }
			}
			# Send messages to rivals allowing them to join
			every_enemy_country = {
				limit = {
					NOT = { tag = A12 }	#Everyone except Beepeck cos they have their own event below
				}
				country_event = { id = flavor_smallcountry.9 days = 1 }
			}
			
			A12 = {	#Beepeck's choice to join or ally
				country_event = { id = flavor_smallcountry.10 days = 1 }
			}
		}
	}
}

# PROVINCENAME Revolts!
province_event = {
	id = flavor_smallcountry.2	#formerly 8
	title = "flavor_smallcountry.2.t"
	desc = "flavor_smallcountry.2.d"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		is_year = 1550
		NOT = { is_year = 1650 }
		owner = { ai = no }
		NOT = { has_global_flag = halfling_revolt }
		NOT = { local_autonomy = 1 }
		region = small_country_region
		OR = {
			culture_group = halfling
			is_core = A97
		}
		owner = {
			NOT = { tag = A97 }
			NOT = { tag = A12 }
			is_free_or_tributary_trigger = yes
			num_of_cities = 6
			capital_scope = {
				NOT = { region = small_country_region }
			}
			NOT = {
				OR = {
					culture_group = halfling
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 4000
		modifier = {
			factor = 0.75
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0.5
			has_owner_religion = no
		}
		modifier = {
			factor = 0.5
			owner = {
				NOT = { culture_group = ROOT }
			}
		}
		modifier = {
			factor = 0.5
			owner = {
				NOT = { primary_culture = ROOT }
				NOT = { accepted_culture = ROOT }
			}
		}
		modifier = {
			factor = 0.5
			owner = { war_with = A97 }
		}
		modifier = {
			factor = 0.75
			unrest = 3
		}
		modifier = {
			factor = 0.75
			unrest = 5
		}
		modifier = {
			factor = 0.75
			unrest = 7
		}
		modifier = {
			factor = 0.75
			unrest = 9
		}
	}

	immediate = {
		set_global_flag = halfling_revolt
	}
	
	option = {
		name = "flavor_smallcountry.2.a"
		set_province_flag = halfling_revolt
		spawn_rebels = {
			type = nationalist_rebels
			size = 3
			friend = A97
		}
	}
	option = {
		name = "flavor_smallcountry.2.b"
		set_province_flag = no_halfling_revolt
		add_local_autonomy = 100
	}
}

# PROVINCENAME Revolts!
province_event = {
	id = flavor_smallcountry.3	#formerly 9
	title = "flavor_smallcountry.3.t"
	desc = "flavor_smallcountry.3.d"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		is_year = 1550
		NOT = { is_year = 1650 }
		owner = { ai = no }
		has_global_flag = halfling_revolt
		NOT = { has_province_flag = halfling_revolt }
		NOT = { has_province_flag = no_halfling_revolt }
		NOT = { local_autonomy = 1 }
		region = small_country_region
		OR = {
			culture_group = halfling
			is_core = A97
		}
		owner = {
			NOT = { tag = A97 }
			NOT = { tag = A12 }
			is_free_or_tributary_trigger = yes
			num_of_cities = 6
			capital_scope = {
				NOT = { region = small_country_region }
			}
			NOT = {
				OR = {
					culture_group = halfling
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 120
		modifier = {
			factor = 0.5
			any_neighbor_province = {
				has_province_flag = halfling_revolt
			}
		}
		modifier = {
			factor = 0.5
			has_owner_religion = no
		}
		modifier = {
			factor = 0.5
			owner = {
				NOT = { culture_group = ROOT }
			}
		}
		modifier = {
			factor = 0.5
			owner = {
				NOT = { primary_culture = ROOT }
				NOT = { accepted_culture = ROOT }
			}
		}
		modifier = {
			factor = 0.5
			owner = { war_with = A97 }
		}
		modifier = {
			factor = 0.75
			unrest = 3
		}
		modifier = {
			factor = 0.75
			unrest = 5
		}
		modifier = {
			factor = 0.75
			unrest = 7
		}
		modifier = {
			factor = 0.75
			unrest = 9
		}
	}

	option = {
		name = "flavor_smallcountry.3.a"
		set_province_flag = halfling_revolt
		spawn_rebels = {
			type = nationalist_rebels
			size = 2
			friend = A97
		}
	}
	option = {
		name = "flavor_smallcountry.3.b"
		set_province_flag = no_halfling_revolt
		add_local_autonomy = 100
	}
}


# Dutch Mobilization
country_event = {
	id = flavor_smallcountry.4	#formerly 11
	title = "flavor_smallcountry.4.t"
	desc = "flavor_smallcountry.4.d"
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "EXCELLENT"
		A97 = {
			capital_scope = {
				build_to_forcelimit = {
					infantry = 0.7
					cavalry = 0.1
					artillery = 0.2
				}
			}
			random_owned_province = {
				limit = {
					has_port = yes
				}
				build_to_forcelimit = {
					heavy_ship = 0.1
					light_ship = 0.3
					transport = 0.1
				}
			}
		}
	}
}

# Dutch Claims
country_event = {
	id = flavor_smallcountry.5	#formerly 12
	title = "flavor_smallcountry.5.t"
	desc = "flavor_smallcountry.5.d"
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "flavor_smallcountry.5.a"
		every_owned_province = {
			limit = {
				region = small_country_region
			}
			add_permanent_claim = A97
		}
	}
}

# Dutch state joins the Netherlands
country_event = {
	id = flavor_smallcountry.6	#formerly 16
	title = "flavor_smallcountry.6.t"
	desc = "flavor_smallcountry.6.d"
	picture = DIPLOMACY_eventPicture
	
	major = yes
	
	trigger = {
		ai = yes
		was_player = no
		NOT = { tag = A97 }
		A97 = {
			exists = yes
			#ai = yes	lets make it so the player can get it too
			was_player = no
		}
		is_free_or_tributary_trigger = yes
		is_at_war = no
		NOT = { num_of_cities = 3 }	#lowers size threshold so they can join
		any_owned_province = {
			region = small_country_region
		}
		OR = {
			culture_group = halfling
		}
		NOT = {
			has_country_flag = state_joined_small_country
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "flavor_smallcountry.6.a"
		A97 = {
			country_event = { id = flavor_smallcountry.7 days = 1 }
		}
		set_country_flag = state_joined_small_country
	}
}

# Incorporated into the Netherlands
country_event = {
	id = flavor_smallcountry.7	#formerly 17
	title = "flavor_smallcountry.7.t"
	desc = "flavor_smallcountry.7.d"
	picture = DIPLOMACY_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "flavor_smallcountry.7.a"
		FROM = {
			every_owned_province = {
				add_core = A97
			}
		}
		inherit = FROM
	}
}

# PROVINCENAME Revolts!
province_event = {
	id = flavor_smallcountry.8	#formerly 18
	title = "flavor_smallcountry.8.t"
	desc = "flavor_smallcountry.8.d"
	picture = ANGRY_MOB_eventPicture
	
	trigger = {
		is_year = 1550
		NOT = { is_year = 1650 }
		A97 = {
			exists = yes
			ai = yes
			was_player = no
		}
		owner = {
			NOT = { tag = A97 }
		}
		region = small_country_region
		OR = {
			culture_group = halfling
			is_core = A97
		}
		OR = {
			NOT = { has_province_flag = halfling_revolt }
			had_province_flag = { flag = halfling_revolt days = 1825 }
		}
		owner = {
			OR = {
				capital_scope = {
					NOT = { region = small_country_region }
				}
				NOT = {
					OR = {
						culture_group = halfling
					}
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 120
		modifier = {
			factor = 0.75
			any_neighbor_province = {
				owned_by = A97
			}
		}
		modifier = {
			factor = 0.75
			any_neighbor_province = {
				has_province_flag = halfling_revolt
			}
		}
		modifier = {
			factor = 0.75
			has_owner_religion = no
		}
		modifier = {
			factor = 0.75
			owner = {
				NOT = { culture_group = ROOT }
			}
		}
		modifier = {
			factor = 0.75
			owner = {
				NOT = { primary_culture = ROOT }
				NOT = { accepted_culture = ROOT }
			}
		}
		modifier = {
			factor = 0.5
			owner = { war_with = A97 }
		}
		modifier = {
			factor = 0.75
			unrest = 3
		}
		modifier = {
			factor = 0.75
			unrest = 5
		}
		modifier = {
			factor = 0.75
			unrest = 7
		}
		modifier = {
			factor = 0.75
			unrest = 9
		}
	}

	option = {
		name = "flavor_smallcountry.8.a"
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				owner = {
					NOT = { num_of_cities = 5 }
				}
			}
		}
		set_province_flag = halfling_revolt
		spawn_rebels = {
			type = nationalist_rebels
			size = 2
			friend = A97
		}
	}
	option = {
		name = "flavor_smallcountry.8.b"
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				owner = {
					num_of_cities = 5
				}
			}
		}
		add_core = A97
		cede_province = A97
	}
}

# Netherland revolters call on the rivals of their enemy
country_event = {
	id = flavor_smallcountry.9	#formerly 20 
	title = "flavor_smallcountry.9.t"
	desc = "flavor_smallcountry.9.d"
	picture = BATTLE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "flavor_smallcountry.9.a"
		create_alliance = A97
		set_ai_attitude = {
			who = A97
			attitude = attitude_friendly
		}
		A97 = {
			add_opinion = {
				who = ROOT
				modifier = supported_independence
			}
		}
	}
	option = {
		name = "flavor_smallcountry.9.b"
		add_prestige = -10
	}
}

# Beepeck's choice whether to join or not
country_event = {
	id = flavor_smallcountry.10	#formerly 20 
	title = "flavor_smallcountry.10.t"
	desc = "flavor_smallcountry.10.d"
	picture = BATTLE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "flavor_smallcountry.10.a"	#Let us join our brothers-in-arms!
		ai_chance = {
			factor = 25
			modifier = {
				factor = 0
				owner = {
					NOT = { num_of_cities = 5 }	#If Beepeck is small enough
				}
			}
		}
		A97 = {
			inherit = A12
		}
		emperor = {	#Emperor is pissed at Small Country
			add_opinion = {
				who = A97
				modifier = beepeck_secession
			}
		}
	}
	option = {
		name = "flavor_smallcountry.10.b"	#An alliance will allow us to aid them from within the Empire of Anbennar
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				owner = {
					num_of_cities = 5	#If Beepeck is strong enough they'll just ally instead
				}
			}
		}
		create_alliance = A97
		set_ai_attitude = {
			who = A97
			attitude = attitude_friendly
		}
		A97 = {
			add_opinion = {
				who = ROOT
				modifier = supported_independence
			}
		}
	}
	option = {
		name = "flavor_smallcountry.10.c"	#So? We're about business, not freedom.
		ai_chance = {
			factor = 25
			modifier = {
				factor = 0
				owner = {
					num_of_cities = 6	#If Beepeck is strong enough they wont need them
				}
			}
		}
		add_prestige = 10
	}
}