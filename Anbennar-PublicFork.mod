name="Anbennar-PublicFork"
path="mod/Anbennar-PublicFork"
tags={"Alternative History""Total Conversion""Fantasy"}

supported_version="1.25.*.*"

#replace_path="localisation/prov_names_l_english"
#replace_path="localisation/prov_names_adj_l_english"


replace_path="common/bookmarks"
replace_path="common/province_names"
replace_path="common/government_names"
replace_path="common/disasters"
replace_path="common/rebel_types"

replace_path="events"
replace_path="missions"
replace_path="decisions"
replace_path="history/diplomacy"

replace_path="history/wars"
replace_path="history/provinces"
picture="anbennar.jpg"
