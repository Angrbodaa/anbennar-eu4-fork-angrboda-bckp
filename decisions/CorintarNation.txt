country_decisions = {

	corintar_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_corintar_flag }
			was_never_end_game_tag_trigger = yes
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			has_reform = adventurer_reform
			tag = B02
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			adm_tech = 7
			tag = B02
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			num_of_cities = 5
		}
		effect = {
			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_adventurers_modifier"
				duration = 14600
			}
			set_country_flag = formed_corintar_flag
			country_event = { id = flavor_corintar.0 days = 1 }
			
			hidden_effect = {
				clr_country_flag = 	knightly_order_adventurer
			}
			set_country_flag = adventurer_derived_government
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
}