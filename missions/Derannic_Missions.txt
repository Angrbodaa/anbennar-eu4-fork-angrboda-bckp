
deranne_slot1 = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = A02
	}
	has_country_shield = yes
	
	deranne_recover_from_lilac_wars = {
		icon = mission_high_income
		required_missions = { }
		position = 1
		
		trigger = {
				stability = 2
			}
			
		effect = {
			add_country_modifier = {
				name = "building_spree"
				duration = 9125
			}
		}
	}
	
	deranne_secure_darom = {
		icon = mission_have_two_subjects
		required_missions = { deranne_recover_from_lilac_wars }
		position = 2

		provinces_to_highlight = {
			area = darom_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			darom_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			shrouded_coast_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	deranne_into_the_shrouded_coast = {
		icon = mission_unite_home_region
		required_missions = { deranne_secure_darom }
		position = 3

		provinces_to_highlight = {
			area = shrouded_coast_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			shrouded_coast_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			sorncost_vines_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	deranne_land_of_the_southern_wine = {
		icon = mission_cannons_firing
		required_missions = { deranne_into_the_shrouded_coast }
		position = 4

		provinces_to_highlight = {
			OR = {
				area = sorncost_vines_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			sorncost_vines_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_country_modifier = {
				name = "hegemonic_ambition"
				duration = 9125 #25 years
			}
		}
	}
	
}

deranne_slot2 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = A02
	}
	has_country_shield = yes
	
	deranne_conquer_southroy = {
		icon = mission_unite_home_region
		required_missions = { deranne_recover_from_lilac_wars }
		position = 2
		
		provinces_to_highlight = {
			OR = {
				area = southroy_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			southroy_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			126 = {
				NOT = { is_core = ROOT }
				NOT = { owned_by = ROOT }
				add_permanent_claim = ROOT
			}
		}
	}
	
	deranne_conquest_of_portnamm = {
		icon = mission_sea_battles
		required_missions = { deranne_conquer_southroy }
		
		
		provinces_to_highlight = {
			province_id = 126
			NOT = { owned_by = ROOT }
		}
		trigger = {
			126 = {
				owned_by = ROOT
			}
		}
		effect = {
			add_country_modifier = {
				name = "merchant_navy"
				duration = 9125 #25 years
			}
		}
	}
}

#Internal
deranne_slot3 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = A02
	}
	has_country_shield = yes
	
	deranne_secure_upper_winebay = {
		icon = mission_assemble_an_army
		required_missions = { deranne_recover_from_lilac_wars }
		position = 2
		
		provinces_to_highlight = {
			OR = {
				area = upper_winebay_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			upper_winebay_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "merchant_navy"
				duration = 5475
			}
		}
	}
	
	deranne_aelvar_manufactory = {
		icon = mission_have_manufactories
		required_missions = { deranne_gateway_to_the_west } 
		position = 3
		
		provinces_to_highlight = {
			province_id = 110
		}
		
		trigger = {
			owns = 110
			110 = {
				has_building = wharf
			}
		}

		effect = {
			110 = {
				add_base_production = 3
			}
			add_country_modifier = {
				name = "deranne_aelvar_wood"
				duration = 5475
			}
		}
	}
	
	deranne_derannic_armada = {
		icon = mission_establish_high_seas_navy
		required_missions = { deranne_aelvar_manufactory }
		position = 4
		
		trigger = { 
			OR = {
				num_of_heavy_ship = 30
			}
		}
		effect = {
			add_country_modifier = {
				name = "naval_enthusiasm_mission"
				duration = 9125 #25 years
			}
		}
	}
}

#Colonial Internal
deranne_slot4 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = A02
	}
	has_country_shield = yes
	
	deranne_gateway_to_the_west = {
		icon = mission_early_game_buildings
		required_missions = { deranne_rediscovery_of_aelantir } 
		position = 2
		
		provinces_to_highlight = {
			province_id = 112
		}
		
		trigger = {
			owns = 112
			112 = {
				province_has_center_of_trade_of_level = 2
				has_estate = no
			}
		}

		effect = {
			112 = {
				add_base_production = 3
				add_base_manpower = 3
				add_base_tax = 3
			}
		}
	}
	
	deranne_establish_derannport = {
		icon = mission_galleys_in_port
		required_missions = { 
			deranne_rediscovery_of_aelantir 
			deranne_gateway_to_the_west
		}
		position = 3
		provinces_to_highlight = {
			region = trollsbay_region
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
			has_discovered = yes
		}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = miss_colonize_endralliande
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					value = 1
					region = trollsbay_region
					is_city = yes
				}
			}
		}
		
		effect = {
			random_owned_province = { 
				limit = { region = trollsbay_region }
				change_province_name = "Derannport"
				rename_capital = "Derannport"
				add_base_production = 2
				add_base_tax = 2
			}
		}
	}
	
	deranne_greater_derannport = {
		icon = mission_sea_battles
		required_missions = { deranne_establish_derannport }
		position = 4
		provinces_to_highlight = {
			colonial_region = colonial_noruin
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
		}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = miss_colonize_isles
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					value = 5
					colonial_region = colonial_noruin
					is_city = yes
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 7300
			}
		}
	}
}

#Colonial
deranne_slot5 = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = A02
	}
	has_country_shield = yes
	
	deranne_rediscovery_of_aelantir = {
		icon = mission_sea_battles
		required_missions = {  }
		position = 1
		trigger = {
			custom_trigger_tooltip = {
				tooltip = aelantir_discovery.tooltip
				colonial_endralliande = {
					has_discovered = ROOT
				}
			}
		}

		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 7300
			}
		}
	}
	
	deranne_colonize_endralliande = {
		icon = mission_dominate_home_trade_node
		required_missions = { deranne_rediscovery_of_aelantir }
		position = 2
		provinces_to_highlight = {
			colonial_region = colonial_endralliande
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
			has_discovered = yes
		}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = miss_colonize_endralliande
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					value = 1
					colonial_region = colonial_endralliande
					is_city = yes
				}
			}
		}
		
		effect = {
			add_prestige = 15
			add_country_modifier = {
				name = "the_unknown_frontier"
				duration = 5475
			}
		}
	}
	
	deranne_colonize_ruined_isles = {
		icon = mission_dominate_home_trade_node
		required_missions = { deranne_colonize_endralliande }
		position = 3
		provinces_to_highlight = {
			colonial_region = colonial_isles
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
			has_discovered = yes
		}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = miss_colonize_isles
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					value = 1
					colonial_region = colonial_isles
					is_city = yes
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 7300
			}
		}
	}
}