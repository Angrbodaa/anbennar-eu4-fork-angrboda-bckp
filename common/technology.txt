# Do not change tags in here without changing every other reference to them.
# If adding new technology, make sure they are uniquely named.


# Groups must be defined BEFORE tables.

#Nation Designer Cost is set to match triggers for starting institutions.

groups = {
	#Cannorian, the most diverse group and not really race-tied, though overwhelmingly human - other races can reform to this if their primary culture provinces < non-primary
	tech_cannorian = {	
		start_level = 3
		start_cost_modifier = 0
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world		#Will need to be updated, this looks like capitals shouldnt be on these continents
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	#dwarves can see provinces nearby but non-dwarves cant see dwarven regions in Serpentspine
	tech_dwarven = {
		start_level = 3
		start_cost_modifier = 0
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_elven = {
		start_level = 3
		start_cost_modifier = 0
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_gnomish = {
		start_level = 3
		start_cost_modifier = 0
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	#dunno about this maybe just keep salahadesi for southern humans
	tech_bulwari = {
		start_level = 3
		start_cost_modifier = 0.25
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	#for southern humans
	tech_salahadesi = {
		start_level = 3
		start_cost_modifier = 0.40
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_harimari = {
		start_level = 3
		start_cost_modifier = 0.5
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	#Haless Humans
	tech_essanese = {
		start_level = 3
		start_cost_modifier = 0.5
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_gnollish = {
		start_level = 2
		start_cost_modifier = 0.75
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_kobold = {
		start_level = 2
		start_cost_modifier = 0.75
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_harpy = {
		start_level = 3
		start_cost_modifier = 0.75
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_orcish = {
		start_level = 2
		start_cost_modifier = 0.75
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_goblin = {
		start_level = 2
		start_cost_modifier = 0.75
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_giantkind = {
		start_level = 2
		start_cost_modifier = 0.75
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	tech_north_aelantiri = {
		start_level = 1
		start_cost_modifier = 2.5
		is_primitive = yes
	}
	tech_south_aelantiri = {
		start_level = 1
		start_cost_modifier = 2.5
		is_primitive = yes
	}
	tech_effelai = {
		start_level = 1
		start_cost_modifier = 2.5
		is_primitive = yes
	}
	tech_eordand = {
		start_level = 3
		start_cost_modifier = 0
	}
	tech_ynnic = {
		start_level = 3
		start_cost_modifier = 0
	}
	tech_kheionai = {
		start_level = 3
		start_cost_modifier = 0
	}
	tech_taychendi = {
		start_level = 3
		start_cost_modifier = 0
	}
	tech_undead = {
		start_level = 2
		start_cost_modifier = 0.75
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	
	western = {
		start_level = 3
		start_cost_modifier = 0
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	eastern = {
		start_level = 3
		start_cost_modifier = 0.20
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	ottoman = {
		start_level = 3
		start_cost_modifier = 0.25
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	muslim = {
		start_level = 3
		start_cost_modifier = 0.40
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	indian = {
		start_level = 3
		start_cost_modifier = 0.5
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	east_african = {
		start_level = 3
		start_cost_modifier = 0.5
		nation_designer_unit_type = sub_saharan
	}
	central_african = {
		start_level = 2
		start_cost_modifier = 0.65
		nation_designer_unit_type = sub_saharan
	}
	chinese = {
		start_level = 3
		start_cost_modifier = 0.6
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
	#Could keep this
	nomad_group = {
		start_level = 3
		start_cost_modifier = 0.75
		nation_designer_trigger = {
				has_reform = steppe_horde
		}
	}
	sub_saharan = { #West African
		start_level = 2
		start_cost_modifier = 0.6
	}
	north_american = {
		start_level = 1
		start_cost_modifier = 2.5
		is_primitive = yes
	}
	mesoamerican = {
		start_level = 1
		start_cost_modifier = 1.5
		is_primitive = yes
	}
	south_american = {
		start_level = 1
		start_cost_modifier = 2.5
		is_primitive = yes
	}
	andean = {
		start_level = 1
		start_cost_modifier = 1.5
		nation_designer_unit_type = south_american
		is_primitive = yes
	}	
	
	
	high_american = {
		start_level = 3
		start_cost_modifier = 0
		nation_designer_unit_type = high_american
		nation_designer_trigger = {
			NOT = {
				capital_scope = {
					continent = europe
				}
			}
		}
		nation_designer_cost = {
			trigger = {
				capital_scope = {
					OR = {
						continent = new_world
						continent = north_america
						continent = south_america
						continent = oceania
					}
				}
			}
			value = 75
		}
	}
}

tables = {
	adm_tech = "technologies/adm.txt"
	dip_tech = "technologies/dip.txt"
	mil_tech = "technologies/mil.txt"
}
