#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 17  141  48 }

revolutionary_colors = { 17  141  48 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	influence_ideas
	defensive_ideas	
	administrative_ideas	
	trade_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Generic Thekvrystana Names
	"Adradar #0" = 50
	"Aldalash #0" = 50
	"Aldaran #0" = 50
	"Alvardip #0" = 50
	"Andrellaran #0" = 50
	"Aranrudu #0" = 50
	"Ardpanatam #0" = 50
	"Ardpanth #0" = 50
	"Artoratam #0" = 50
	"Calindash #0" = 50
	"Camnan #0" = 50
	"Camnaran #0" = 50
	"Camnavna #0" = 50
	"Darakash #0" = 50
	"Daranran #0" = 50
	"Denariatam #0" = 50
	"Dorenpan #0" = 50
	"Ebenanth #0" = 50
	"Eboranbu #0" = 50
	"Elajith #0" = 50
	"Eletam #0" = 50
	"Elrandip #0" = 50
	"Elrathikash #0" = 50
	"Eranbu #0" = 50
	"Eranpin #0" = 50
	"Erenlash #0" = 50
	"Erlandip #0" = 50
	"Ervram #0" = 50
	"Ervramesh #0" = 50
	"Evinran #0" = 50
	"Filikash #0" = 50
	"Finoratam #0" = 50
	"Galinpin #0" = 50
	"Geledish #0" = 50
	"Gelmarathi #0" = 50
	"Jahlonand #0" = 50
	"Jahlorali #0" = 50
	"Jalenesh #0" = 50
	"Jalinwathi #0" = 50
	"Munapin #0" = 50
	"Olorlash #0" = 50
	"Pelojith #0" = 50
	"Serondip #0" = 50
	"Seronpin #0" = 50
	"Seronrin #0" = 50
	"Tailajith #0" = 50
	"Tailananth #0" = 50
	"Tailarudu #0" = 50
	"Teshalashothi #0" = 50
	"Teshalropan #0" = 50
	"Teshapin #0" = 50
	"Teshejith #0" = 50
	"Teshelapan #0" = 50
	"Thaladish #0" = 50
	"Thanwathi #0" = 50
	"Thelriatam #0" = 50
	"Thirendep #0" = 50
	"Thirendish #0" = 50
	"Threzarathi #0" = 50
	"Trianran #0" = 50
	"Ultawathi #0" = 50
	"Urakash #0" = 50
	"Varamjith #0" = 50
	"Varamlash #0" = 50
	"Varampin #0" = 50
	"Varandip #0" = 50
	"Varanran #0" = 50
	"Varelrudu #0" = 50
	"Vatesheran #0" = 50
	"Vinran #0" = 50
		
	"Aladewatha #0" = -10
	"Alanawatha #0" = -10
	"Alaricha #0" = -10
	"Alerchana #0" = -10
	"Amarchana #0" = -10
	"Ariadipa #0" = -10
	"Arichana #0" = -10
	"Calsithara #0" = -10
	"Camdipa #0" = -10
	"Celajitha #0" = -10
	"Devenasha #0" = -10
	"Eburiwathi #0" = -10
	"Elaricha #0" = -10
	"Elechana #0" = -10
	"Erlandipa #0" = -10
	"Erumarai #0" = -10
	"Filinarothi #0" = -10
	"Galiwatha #0" = -10
	"Imatemrana #0" = -10
	"Isehricha #0" = -10
	"Iserana #0" = -10
	"Iserdipa #0" = -10
	"Istralwatha #0" = -10
	"Ivranarha #0" = -10
	"Ivranarhi #0" = -10
	"Jexipina #0" = -10
	"Ladrana #0" = -10
	"Lanahasha #0" = -10
	"Lanorana #0" = -10
	"Lelijitha #0" = -10
	"Leslarasha #0" = -10
	"Liandarha #0" = -10
	"Merialavu #0" = -10
	"Mihtratama #0" = -10
	"Narajitha #0" = -10
	"Nathajitha #0" = -10
	"Selurana #0" = -10
	"Seronchana #0" = -10
	"Sharinarha #0" = -10
	"Sharipina #0" = -10
	"Sheriwathi #0" = -10
	"Shiarthi #0" = -10
	"Taneliata #0" = -10
	"Thalika #0" = -10
	"Valawathi #0" = -10
	"Varidipa #0" = -10
	"Varinjitha #0" = -10
	"Varirana #0" = -10
	"Vehachana #0" = -10
	"Veharathna #0" = -10
	"Vesimathi #0" = -10
	"Zalerjitha #0" = -10
	
}

leader_names = {
	Kartha
	Karthavu
	Kaimal
	Nedungadi
	Thampi
	Nambiar
	Varma
	Vellodi
	Kesavadas
	Naduvazhi
	Kurup
}

ship_names = {
	Amirtha Amman Arul Chellam Devi
	Durga Gayatri Iniya Kaveri Lakshmi
	Madhu Meenakshi Nalini Palar Parvati
	Pennar Ponnaiyar Ponni Radha Ratri
	Sarasvati Sita Tamarai Vaigai
}

army_names = {
	"Thekvrystana Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Thekvrystana Fleet"
}