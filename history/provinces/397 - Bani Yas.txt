#397 - Beni Yas

owner = F06
controller = F06
add_core = F06
culture = desha
religion = mother_akan

hre = no

base_tax = 3
base_production = 5
base_manpower = 2

trade_goods = iron

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
