#472 - Zhetyru

owner = F02
controller = F02
add_core = F02
add_core = F01
culture = kheteratan
religion = khetist

hre = no

base_tax = 5
base_production = 2
base_manpower = 3

trade_goods = cloth

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari