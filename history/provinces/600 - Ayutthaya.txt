# No previous file for Ayutthaya
owner = F44
controller = F44
add_core = F44
culture = zanite
religion = bulwari_sun_cult

hre = no

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = glass

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari