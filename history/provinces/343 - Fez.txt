#343 - Fez |

owner = A13
controller = A13
add_core = A13
add_core = A50
culture = old_alenic
religion = regent_court

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish