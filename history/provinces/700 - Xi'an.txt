# No previous file for Xi'an
owner = Z29
controller = Z29
add_core = Z29
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 5
base_production = 4
base_manpower = 2

trade_goods = fur

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold