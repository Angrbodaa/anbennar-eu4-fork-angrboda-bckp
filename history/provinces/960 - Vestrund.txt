# No previous file for Vestrund
owner = Z10
controller = Z10
add_core = Z10
culture = west_dalr
religion = skaldhyrric_faith

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = fish

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind