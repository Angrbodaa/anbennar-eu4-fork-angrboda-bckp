# 67 - Franken

owner = A01
controller = A01
add_core = A01
culture = high_lorentish
religion = regent_court
base_tax = 12
base_production = 12
trade_goods = cloth #Fashion Capital
center_of_trade = 2
base_manpower = 7
capital = ""
is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}

add_permanent_province_modifier = {
	name = halfling_minority_coexisting_large
	duration = -1
}

add_permanent_province_modifier = {
	name = dwarven_minority_integrated_small
	duration = -1
}

add_permanent_province_modifier = {
	name = gnomish_minority_integrated_small
	duration = -1
}