# No previous file for Caffa
owner = A77
controller = A77
add_core = A77
culture = vernman
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 6

trade_goods = grain

capital = ""

is_city = yes

fort_15th = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish