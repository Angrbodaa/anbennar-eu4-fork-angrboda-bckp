#337 - Oran |

owner = A13
controller = A13
add_core = A13
add_core = A51
culture = gawedi
religion = regent_court

hre = no

base_tax = 7
base_production = 6
base_manpower = 4

trade_goods = cloth
center_of_trade = 2

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
discovered_by = tech_orcish
