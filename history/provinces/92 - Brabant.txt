#92 - Brabant | East Ording

owner = A17
controller = A17
add_core = A17
culture = low_lorentish
religion = regent_court
hre = no
base_tax = 3
base_production = 2
trade_goods = livestock
base_manpower = 4
capital = ""
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

