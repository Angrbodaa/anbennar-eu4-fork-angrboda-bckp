# 97 Holland - Temple of Divine Succor

owner = A10
controller = A10
add_core = A10
culture = high_lorentish
religion = regent_court
hre = no
base_tax = 5
base_production = 12
trade_goods = wine
base_manpower = 2
capital = "Minara's Bosom"
is_city = yes
fort_15th = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
