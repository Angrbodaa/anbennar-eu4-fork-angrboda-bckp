#59 - Wittenburg | 

owner = A01
controller = A01
add_core = A01
add_core = A78
culture = roilsardi
religion = regent_court

hre = yes

base_tax = 5
base_production = 7
base_manpower = 5

trade_goods = wine

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish