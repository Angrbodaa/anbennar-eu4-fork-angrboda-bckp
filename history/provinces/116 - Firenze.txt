#116 - Firenze

owner = A03
controller = A03
add_core = A03
culture = moon_elf
religion = regent_court
hre = no
base_tax = 4
base_production = 6
trade_goods = silk
base_manpower = 2
capital = ""
is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
