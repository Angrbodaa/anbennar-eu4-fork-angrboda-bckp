government = monarchy
add_government_reform = feudalism_reform
government_rank = 2
primary_culture = busilari
religion = regent_court
technology_group = tech_cannorian
capital = 408
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1367.6.12 = {
	monarch = {
		name = "Marcan I"
		dynasty = "Silnara"
		birth_date = 1366.2.3
		adm = 2
		dip = 3
		mil = 0
	}
}

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1428.5.5 = {
	heir = {
		name = "Ardan"
		monarch_name = "Ardan III"
		dynasty = "Silnara"
		birth_date = 1427.5.5
		death_date = 1499.4.4
		claim = 95
		adm = 4
		dip = 6
		mil = 1
	}
}